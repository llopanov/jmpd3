package figures;

/**
 * Created by antar on 12.02.2017.
 */
abstract class AbstractFigure implements Volumable{
    protected String myName;
    protected double volume;

    public double getVolume(){
        return volume;
    }

    @Override
    public String toString() {
        return "I am " + myName + " my volume is " +volume;
    }
}
