package figures;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antar on 12.02.2017.
 */
public class ComplexFigure extends AbstractFigure{
    private List<Volumable> list = new ArrayList<Volumable>();

    public ComplexFigure() {
        this.myName = "ComplexFigure";
        this.volume = 0;
    }

    public void addFigure(Volumable figure){
        list.add(figure);
        this.volume += figure.getVolume();
    }
}
