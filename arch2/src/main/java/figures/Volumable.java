package figures;

/**
 * Created by antar on 12.02.2017.
 */
public interface Volumable {
    double getVolume();
}
