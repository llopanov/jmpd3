package figures;

/**
 * Created by antar on 12.02.2017.
 */
public class Sphere extends AbstractFigure{
    private double r;

    public Sphere(double r) {
        this.myName = "Sphere";
        this.r = r;
        this.volume = 4*Math.PI*r*r*r/3;
    }
}
