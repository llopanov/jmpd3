package figures;

/**
 * Created by antar on 12.02.2017.
 */
public class Parallelepiped extends AbstractFigure{
    private double dx;
    private double dy;
    private double dz;

    public Parallelepiped(double dx, double dy, double dz) {
        this.myName = "Parallelepiped";
        this.dx = dx;
        this.dy = dy;
        this.dz = dz;
        this.volume = dx*dy*dz;
    }
}
