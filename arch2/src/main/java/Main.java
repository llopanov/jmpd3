import figures.ComplexFigure;
import figures.Parallelepiped;
import figures.Sphere;

/**
 * Created by antar on 11.02.2017.
 */
public class Main {

    public static void main(String[] args) {
        ComplexFigure complexFigure = new ComplexFigure();
        complexFigure.addFigure(new Sphere(5));
        complexFigure.addFigure(new Parallelepiped(10, 5, 5));

        System.out.println("volume is " + complexFigure.getVolume());


    }
}
