package com.judge.logger;

/**
 * Created by Aliaksei_Lapanau on 23.05.2017.
 */
        import org.apache.logging.log4j.LogManager;
        import org.apache.logging.log4j.Logger;

public class LoggerClass {
    public static final Logger LOGGER = LogManager.getRootLogger();
}