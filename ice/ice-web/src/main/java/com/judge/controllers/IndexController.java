package com.judge.controllers;

/**
 * Created by Aliaksei_Lapanau on 08.05.2017.
 */

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class IndexController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        System.out.println("index");
        model.addAttribute("message", "Spring 3 MVC - Hello World");
        return "index";

    }



    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(ModelMap model) {
        System.out.println("login");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {

            // The user is logged in
            return "forward:/admin/index";
        }

        //com.judge.model.addAttribute("message", "Spring 3 MVC - Hello World");
        return "login";

    }

}