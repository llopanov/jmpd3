package com.judge.controllers.validators;

import com.judge.model.Element;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import org.springframework.validation.Validator;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */
public class ElementFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Element.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Element element = (Element)target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "","name is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "baseValue", "", "baseValue is empty");

        if (element.getName().length()>50) {
            errors.rejectValue("name","", "Name should be less 50 characters");
        }

        if (element.getBaseValue() <= 0) {
            errors.rejectValue("baseValue","", "wrong base value for element");
        }
    }
}
