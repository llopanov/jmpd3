package com.judge.controllers;

import com.judge.controllers.validators.UserFormValidator;
import com.judge.logger.LoggerClass;
import com.judge.model.User;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import com.judge.services.interfaces.IUserService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
@Controller
public class UserController {

//    @Autowired
//    private  Validator userFormValidator; //UserFormValidator

    @Autowired
    private IUserService userService;// = new UserService();

    private static final String LIST_USERS_PATH = "admin/user/list";
    private static final String ADD_EDIT_USER_PATH = "admin/user/userForm";

//    @InitBinder//("user")
//    public void dataBinding(WebDataBinder binder) {
//        binder.addValidators(userFormValidator);//, emailValidator);
////        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
////        dateFormat.setLenient(false);
////        binder.registerCustomEditor(Date.class, "dob", new CustomDateEditor(dateFormat, true));
//    }

    @RequestMapping(path = "/admin/users", method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        List<User> list = userService.getListUsers();
        model.addAttribute("listUsers", list);
        return LIST_USERS_PATH;
    }

    @RequestMapping(path = "/admin/users/delete/id/{id}",  method = RequestMethod.GET)
    public String deleteAction(@PathVariable int id, ModelMap model) {
        userService.deleteUser(id);
        return "redirect:/admin/users";
    }

    @RequestMapping(path = "/admin/users/add",  method = RequestMethod.GET)
    public String addPage(ModelMap model) {
        model.addAttribute("user", new User());
        model.addAttribute("listUserRoles", userService.getUserRoles());
        return ADD_EDIT_USER_PATH;
    }

    @RequestMapping(path = "/admin/users/add",  method = RequestMethod.POST)
    public String addAction(@ModelAttribute @Valid User user, BindingResult result, ModelMap model) {

        //run Spring validator manually
        new UserFormValidator().validate(user, result);
        if(result.hasErrors()) {
            model.addAttribute("errors", result);
            model.addAttribute("listUserRoles", userService.getUserRoles());
            return ADD_EDIT_USER_PATH;
        }

        if (user.getId_user() != 0){
            userService.updateUser(user);
        }
        else {
            boolean noErrors = userService.createUser(user);
            if (!noErrors){
                result.rejectValue("login", "","login already exists");
                model.addAttribute("errors", result);
                model.addAttribute("listUserRoles", userService.getUserRoles());
                return ADD_EDIT_USER_PATH;
            }
        }

        return "redirect:/admin/users";//"forward:/admin/users";
//        List<User> list = userService.getListUsers();
//        model.addAttribute("listUsers", list);
//
//        return LIST_USERS_PATH;

    }

    @RequestMapping(path = "/admin/users/edit/id/{id}",  method = RequestMethod.GET)
    public String editAction(@PathVariable int id, ModelMap model) {
        LoggerClass.LOGGER.log(Level.DEBUG, "edit action id = "+id);
        User user = userService.readUser(id);
        model.addAttribute("listUserRoles", userService.getUserRoles());
        model.addAttribute("user", user);
        return ADD_EDIT_USER_PATH;
    }
}
