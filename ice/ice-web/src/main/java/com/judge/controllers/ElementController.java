package com.judge.controllers;

import com.judge.controllers.validators.ElementFormValidator;
import com.judge.logger.LoggerClass;
import com.judge.model.Element;
import com.judge.model.User;
import com.judge.services.interfaces.IElementService;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */
@Controller
public class ElementController {

    @Autowired
    IElementService elementService;
    private static final String LIST_ELEMENTS_PATH = "admin/element/list";
    private static final String ADD_EDIT_ELEMENT_PATH = "admin/element/elementForm";

    @RequestMapping(path = "/admin/elements", method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        List<Element> list = elementService.getListElements();
        model.addAttribute("listElements", list);
        return LIST_ELEMENTS_PATH;
    }

    @RequestMapping(path = "/admin/elements/add",  method = RequestMethod.GET)
    public String addPage(ModelMap model) {
        model.addAttribute("element", new Element());
        return ADD_EDIT_ELEMENT_PATH;
    }

    @RequestMapping(path = "/admin/elements/delete/id/{id}",  method = RequestMethod.GET)
    public String deleteAction(@PathVariable int id, ModelMap model) {
        elementService.deleteElement(id);
        return "redirect:/admin/elements";
    }
    @RequestMapping(path = "/admin/elements/add",  method = RequestMethod.POST)
    public String addAction(@ModelAttribute @Valid Element element, BindingResult result, ModelMap model) {

        //run Spring validator manually
        new ElementFormValidator().validate(element, result);
        if(result.hasErrors()) {
            model.addAttribute("errors", result);
            return ADD_EDIT_ELEMENT_PATH;
        }

        if (element.getId_element() != 0){
            elementService.updateElement(element);
        }
        else {
            elementService.createElement(element);
        }

        return "redirect:/admin/elements";

    }

    @RequestMapping(path = "/admin/elements/edit/id/{id}",  method = RequestMethod.GET)
    public String editAction(@PathVariable int id, ModelMap model) {
        LoggerClass.LOGGER.log(Level.DEBUG, "edit action id = "+id);
        Element element = elementService.readElement(id);
        model.addAttribute("element", element);
        return ADD_EDIT_ELEMENT_PATH;
    }
}
