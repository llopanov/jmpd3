package com.judge.controllers.validators;

import com.judge.model.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Aliaksei_Lapanau on 20.05.2017.
 */
public class UserFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User)target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "","Login is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "", "Password is empty");
        if (user.getLogin().length()<5) {
            errors.rejectValue("login","", "Login length is less than 5");
        }
    }
}
