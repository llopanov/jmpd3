package com.judge.controllers;

import com.judge.controllers.validators.UserFormValidator;
import com.judge.logger.LoggerClass;
import com.judge.model.Participant;
import com.judge.model.User;
import com.judge.services.interfaces.IParticipantService;
import com.judge.services.interfaces.IUserService;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
@Controller
public class ParticipantController {

//    @Autowired
//    private  Validator userFormValidator; //UserFormValidator

    @Autowired
    private IParticipantService participantService;

    private static final String LIST_PARTICIPANT_PATH = "admin/participant/list";
    private static final String ADD_EDIT_PARTICIPANT_PATH = "admin/participant/participantForm";

//    @InitBinder//("user")
//    public void dataBinding(WebDataBinder binder) {
//        binder.addValidators(userFormValidator);//, emailValidator);
////        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
////        dateFormat.setLenient(false);
////        binder.registerCustomEditor(Date.class, "dob", new CustomDateEditor(dateFormat, true));
//    }

    @RequestMapping(path = "/admin/participant", method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        List<Participant> list = participantService.getListParticipants();
        model.addAttribute("listParticipants", list);
        return LIST_PARTICIPANT_PATH;
    }

//    @RequestMapping(path = "/admin/users/delete/id/{id}",  method = RequestMethod.GET)
//    public String deleteAction(@PathVariable int id, ModelMap model) {
//        participantService.deleteParticipant(id);
//        return "redirect:/admin/users";
//    }
//
    @RequestMapping(path = "/admin/participant/add",  method = RequestMethod.GET)
    public String addPage(ModelMap model) {
        LoggerClass.LOGGER.log(Level.DEBUG, "add new participant");
        model.addAttribute("participant", new Participant());
        //model.addAttribute("listUserRoles", participantService.getListParticipants()UserRoles());
        return ADD_EDIT_PARTICIPANT_PATH;
    }
//
//    @RequestMapping(path = "/admin/users/add",  method = RequestMethod.POST)
//    public String addAction(@ModelAttribute @Valid Participant participant, BindingResult result, ModelMap model) {
//
//        //run Spring validator manually
//        new UserFormValidator().validate(user, result);
//        if(result.hasErrors()) {
//            model.addAttribute("errors", result);
//            model.addAttribute("listUserRoles", participantService.getUserRoles());
//            return ADD_EDIT_PARTICIPANT_PATH;
//        }
//
//        if (user.getId_user() != 0){
//            participantService.updateParticipant(user);
//        }
//        else {
//            boolean noErrors = participantService.createParticipant(user);
//            if (!noErrors){
//                result.rejectValue("login", "","login already exists");
//                model.addAttribute("errors", result);
//                model.addAttribute("listUserRoles", participantService.getUserRoles());
//                return ADD_EDIT_PARTICIPANT_PATH;
//            }
//        }
//
//        return "redirect:/admin/users";//"forward:/admin/users";
//    }

//    @RequestMapping(path = "/admin/users/edit/id/{id}",  method = RequestMethod.GET)
//    public String editAction(@PathVariable int id, ModelMap model) {
//        LoggerClass.LOGGER.log(Level.DEBUG, "edit action id = "+id);
//        User user = userService.readUser(id);
//        model.addAttribute("listUserRoles", userService.getUserRoles());
//        model.addAttribute("user", user);
//        return ADD_EDIT_USER_PATH;
//    }
}
