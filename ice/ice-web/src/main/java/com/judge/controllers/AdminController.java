package com.judge.controllers;

import static com.judge.logger.LoggerClass.LOGGER;

import org.apache.logging.log4j.Level;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */
@Controller
public class AdminController {

    public static final String ADMIN_INDEX_PAGE = "admin/index";
    //public static final String ADMIN_USERS_PAGE = "admin/users";
    //public static final String ADMIN_ELEMENTS_PAGE = "admin/elements";

    @RequestMapping(value = "/admin/index", method = RequestMethod.GET)
    public String adminIndexPage(ModelMap model) {
        LOGGER.log(Level.DEBUG, ADMIN_INDEX_PAGE);
        return ADMIN_INDEX_PAGE;

    }

//    @RequestMapping(value = ADMIN_USERS_PAGE, method = RequestMethod.GET)
//    public String adminUserPage(ModelMap model) {
//        LOGGER.log(Level.DEBUG, ADMIN_USERS_PAGE);
//        return ADMIN_USERS_PAGE;
//
//    }
//
//    @RequestMapping(value = ADMIN_ELEMENTS_PAGE, method = RequestMethod.GET)
//    public String adminElementPage(ModelMap model) {
//        LOGGER.log(Level.DEBUG, ADMIN_ELEMENTS_PAGE);
//        return ADMIN_ELEMENTS_PAGE;
//
//    }
}
