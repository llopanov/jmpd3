/**
 * Created by Aliaksei_Lapanau on 20.08.2017.
 */
$(document).ready(function () {
    $('#addParticipant').click(function (e) {
        console.log("hello js");
        $('#popUp .modal-error').html();
        $.ajax({
            type: "GET",
            url: "participant/add",
            data: {
                "Id" : 0
            },
            success: function(response){
                // $('#popUp').css("display", "block");
                // $('#popUp .content').html(response);
                console.log("success");
                //console.log(response);
                $('#popUp .modal-body').html(response);
                $('#popUp').modal('show');
            },
            error: function(response){
                // $('#popUp .modal-error').html("Error on server");
                // $('#errorMsg').show();
                console.log("error");
                $('#popUp .modal-error').html(response);
                $('#popUp').modal('show');

            }
        });

    });
});