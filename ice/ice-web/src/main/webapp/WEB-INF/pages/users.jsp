<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Aliaksei_Lapanau
  Date: 14.05.2017
  Time: 22:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>

<h1>User list</h1>
<div><a href="<c:url value="/admin/index"/>">back</a></div>
<div><a href="<c:url value="/admin/users/add"/>">Add user</a></div>

<table>
    <c:forEach var = "user" items="${listUsers}">
    <tr>
        <td>
                ${user.id_user}
        </td>
        <td>
                ${user.login}
        </td>
        <td>
                ${user.role}
        </td>
        <td>
            <a href="<c:url value="/admin/users/delete/id/${user.id_user}" />">delete</a>
        </td>
        <td>
            <a href="<c:url value="/admin/users/edit/id/${user.id_user}" />">edit</a>
        </td>
    </tr>
    </c:forEach>
</table>

</body>
</html>
