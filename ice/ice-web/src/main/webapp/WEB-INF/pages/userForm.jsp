<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Aliaksei_Lapanau
  Date: 14.05.2017
  Time: 22:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<br>

<h1>User list</h1>


<form action="<c:url value="/admin/users/add"/>" method="post">
    <input type="hidden" name="id_user" value="${user.id_user}">
    <table>
        <tr>
            <td>
                 login
            </td>
            <td>
                <input type="text" name="login" value="${user.login}">

            </td>
            <td>
                <c:if test="${errors.hasFieldErrors('login')}">
                    <div style="color: #f00">
                        <c:forEach var = "error" items="${errors.getFieldErrors('login')}">
                            <div>${error.getDefaultMessage()}</div>
                        </c:forEach>
                    </div>
                </c:if>
            </td>
        </tr>
        <tr>
            <td>
                password
            </td>
            <td>
                <input type="password" name="password" value="${user.password}">
            </td>
            <td>
                <c:if test="${errors.hasFieldErrors('password')}">
                    <div style="color: #f00">
                        <c:forEach var = "error" items="${errors.getFieldErrors('password')}">
                            <div>${error.getDefaultMessage()}</div>
                        </c:forEach>
                    </div>
                </c:if>
            </td>
        </tr>
        <tr>
            <td>
                role
            </td>
            <td>
                <select  name="role">
                    <c:forEach var = "userRole" items="${listUserRoles}">
                        <option value="${userRole}" <c:if test="${userRole == user.role}">selected</c:if> >${userRole}</option>
                    </c:forEach>
                </select >

            </td>
            <td></td>
        </tr>
    </table>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<input type="submit">
</form>


</body>
</html>
