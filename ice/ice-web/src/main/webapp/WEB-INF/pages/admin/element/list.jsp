<%--
  Created by IntelliJ IDEA.
  User: Aliaksei_Lapanau
  Date: 28.05.2017
  Time: 21:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Element list</h1>
<div><a href="<c:url value="/admin/index"/>">back</a></div>
<a href="<c:url value="/admin/elements/add"/>">Add element</a>
<table class="table">
    <c:forEach var = "element" items="${listElements}">
        <tr>
            <td>
                    ${element.id_element}
            </td>
            <td>
                    ${element.name}
            </td>
            <td>
                    ${element.baseValue}
            </td>
            <td>
                <a href="<c:url value="/admin/elements/delete/id/${element.id_element}" />">delete</a>
            </td>
            <td>
                <a href="<c:url value="/admin/elements/edit/id/${element.id_element}" />">edit</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
