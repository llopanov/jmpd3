<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Participant form</h1>

<form action="<c:url value="/admin/participant/add"/>" method="post" >
    <input type="hidden" name="id_user" value="${participant.id_participant}">

    <div class="form-group row">
        <label for="participantName" class="col-sm-4 col-form-label">Participant name</label>
        <div class="col-sm-8">
        <input type="text" class="form-control"  name="name" id="participantName" value="${participant.name}">
        </div>
    </div>

    <div class="form-group row">

        <c:if test="${not empty participant.photo}">
            <div>${participant.photo}</div>
        </c:if>

        <c:if test="${empty participant.photo}">
            <label for="participantPhoto" class="col-sm-4 col-form-label">Participant photo</label>
            <div class="col-sm-8">
            <input type="file" class="form-control-file" name="name" id="participantPhoto" value="${participant.photo}">
            </div>
        </c:if>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" class="btn btn-primary">

</form>
