<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>User list</h1>
<div><a href="<c:url value="/admin/index"/>">back</a></div>
<div><a href="<c:url value="/admin/participant/add"/>" >Add participant</a></div>
<div id="addParticipant">add partisipant JSON</div>
<table class="table">
    <c:forEach var = "participant" items="${listParticipants}">
        <tr>
            <td>
                    ${participant.id_participant}
            </td>
            <td>
                    ${participant.name}
            </td>
            <td>
                    ${participant.photo}
            </td>
            <td>
                <a href="<c:url value="/admin/participants/delete/id/${participant.id_participant}" />">delete</a>
            </td>
            <td>
                <a href="<c:url value="/admin/participants/edit/id/${participant.id_participant}" />">edit</a>
            </td>
        </tr>
    </c:forEach>
</table>
