<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Aliaksei_Lapanau
  Date: 14.05.2017
  Time: 22:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Elements</title>
</head>
<br>

<h1>Element list</h1>


<form action="<c:url value="/admin/elements/add"/>" method="post">
    <input type="hidden" name="id_element" value="${element.id_element}">
    <table>
        <tr>
            <td>
                 name
            </td>
            <td>
                <input type="text" name="name" value="${element.name}">

            </td>
            <td>
                <c:if test="${errors.hasFieldErrors('name')}">
                    <div style="color: #f00">
                        <c:forEach var = "error" items="${errors.getFieldErrors('name')}">
                            <div>${error.getDefaultMessage()}</div>
                        </c:forEach>
                    </div>
                </c:if>
            </td>
        </tr>
        <tr>
            <td>
                base value
            </td>
            <td>
                <input type="text" name="baseValue" value="${element.baseValue}">
            </td>
            <td>
                <c:if test="${errors.hasFieldErrors('baseValue')}">
                    <div style="color: #f00">
                        <c:forEach var = "error" items="${errors.getFieldErrors('baseValue')}">
                            <div>${error.getDefaultMessage()}</div>
                        </c:forEach>
                    </div>
                </c:if>
            </td>
        </tr>

    </table>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<input type="submit" value="add">
</form>


</body>
</html>
