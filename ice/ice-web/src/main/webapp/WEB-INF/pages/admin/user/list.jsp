<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>User list</h1>
<div><a href="<c:url value="/admin/index"/>">back</a></div>
<div><a href="<c:url value="/admin/users/add"/>">Add user</a></div>

<table class="table">
    <c:forEach var = "user" items="${listUsers}">
        <tr>
            <td>
                    ${user.id_user}
            </td>
            <td>
                    ${user.login}
            </td>
            <td>
                    ${user.role}
            </td>
            <td>
                <a href="<c:url value="/admin/users/delete/id/${user.id_user}" />">delete</a>
            </td>
            <td>
                <a href="<c:url value="/admin/users/edit/id/${user.id_user}" />">edit</a>
            </td>
        </tr>
    </c:forEach>
</table>
