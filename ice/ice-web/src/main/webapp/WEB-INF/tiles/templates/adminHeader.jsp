<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
        mysite
    </a>
    <div class="collapse navbar-collapse justify-content-between">
        <ul class="navbar-nav  ">
            <li class="nav-item">
                <a class="nav-link" href="<c:url value="/admin/users" />">users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:url value="/admin/participant" />">participants</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:url value="/admin/elements" />">elements</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:url value="/admin/competitions" />">competitions</a>
            </li>
        </ul>

        <ul class="navbar-nav ">
            <li>
                <form class="form-inline my-2 my-lg-0" action="<c:url value="/logout"/>" method="post" id="logoutForm">
                    <input type="hidden"
                           name="${_csrf.parameterName}"
                           value="${_csrf.token}" />
                </form>

                <script>
                    function formSubmit() {
                        document.getElementById("logoutForm").submit();
                    }
                </script>

                <c:if test="${pageContext.request.userPrincipal.name != null}">

                    You are ${pageContext.request.userPrincipal.name} <a  href="javascript:formSubmit()"> Logout</a>

                </c:if>

            </li>
        </ul>

    </div>
</div>