package com.judge.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by Aliaksei_Lapanau on 09.05.2017.
 */
@Configuration
@EnableWebSecurity
//@ComponentScan({"com.judge.com.judge.services", "com.judge.model"})
//@EnableGlobalMethodSecurity(securedEnabled=true)
@ComponentScan("com.judge")//({"com.judge.config", "com.judge.controllers", "com.judge.com.judge.services", "com.judge.model"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService appUserDetailsService;// = new AppUserDetailsService();

    //@Autowired
    //private MyAppBasicAuthenticationEntryPoint myAppBasicAuthenticationEntryPoint;
//    @Bean
//    public UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("user").password("1234").roles("ADMIN").build());
//        //auth.inMemoryAuthentication().withUser("admin").password("123456").roles("ADMIN");
//        //auth.inMemoryAuthentication().withUser("dba").password("123456").roles("DBA");
//        return manager;
//    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("admin").password("1234").roles("ADMIN");
        auth.userDetailsService(appUserDetailsService);//.passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
                //.antMatchers("/dba/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_DBA')")
//                .antMatchers("/resources/**", "/signup", "/about").permitAll()                  2
//                .antMatchers("/admin/**").hasRole("ADMIN")                                      3
//                .antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')")            4
//                .anyRequest().authenticated()                                                   5
                .and()
                    .formLogin().loginPage("/login").failureUrl("/login?error").defaultSuccessUrl("/admin/index", true)
                    .usernameParameter("username").passwordParameter("password")
                .and()
                    .logout().logoutSuccessUrl("/")
                .and()
                    .csrf();

//        http
//                .authorizeRequests()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .permitAll();
//                //.and()
//                //.httpBasic();
    }
}
