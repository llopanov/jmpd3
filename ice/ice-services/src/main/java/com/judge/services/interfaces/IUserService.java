package com.judge.services.interfaces;

import com.judge.model.User;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 21.05.2017.
 */
public interface IUserService {
    //boolean addUser(String login, String password, UserRole type);

    List<User> getListUsers();

    boolean createUser(User user);
    User readUser(int id);
    void updateUser(User user);
    void deleteUser(int id);
    List getUserRoles();

}
