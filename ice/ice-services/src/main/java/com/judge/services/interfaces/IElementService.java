package com.judge.services.interfaces;

import com.judge.model.Element;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */

public interface IElementService {
    List<Element> getListElements();
    boolean createElement(Element element);
    Element readElement(int id);
    void updateElement(Element element);
    void deleteElement(int id);
}
