package com.judge.services;

import com.judge.model.DAO.IUserDAO;
import com.judge.model.User;
import com.judge.model.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.judge.services.interfaces.IUserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
@Service
public class UserService implements IUserService{
    @Autowired
    private IUserDAO userDAO;// = new IUserDbDAOImpl();//new IUserDAOImpl();

//    @Override
//    public synchronized boolean addUser(String login, String password, UserRole type){
//        if(userDAO.readUser(login) != null){
//            return false;
//        }
//        User user = new User(login, password, type);
//        userDAO.createUser(user);
//        return true;
//    }


    @Override
    public List<User> getListUsers() {
        return userDAO.getListUsers();
    }

    @Override
    public synchronized boolean createUser(User user) {
        if(userDAO.readUser(user.getLogin()) != null){
            return false;
        }
        //User user = new User(login, password, type);
        userDAO.createUser(user);
        return true;

        //userDAO.createUser(user);
    }

    @Override
    public User readUser(int id){
        return userDAO.readUser(id);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    @Override
    public void deleteUser(int id) {
        userDAO.delUser(id);
    }


    @Override
    public List<String> getUserRoles() {
        List<String> listRoles = new ArrayList<>();
        for (UserRole role : UserRole.values()){
            listRoles.add(role.toString());
        }
        return listRoles;
    }
}
