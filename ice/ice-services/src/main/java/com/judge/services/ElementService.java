package com.judge.services;

import com.judge.model.DAO.IElementDAO;
import com.judge.model.Element;
import com.judge.services.interfaces.IElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */

@Service
public class ElementService implements IElementService{
    @Autowired
    private IElementDAO elementDAO;

    @Override
    public List<Element> getListElements() {
        return elementDAO.getListElements();
    }

    @Override
    public boolean createElement(Element element) {
        elementDAO.createElement(element);
        return true;
    }

    @Override
    public Element readElement(int id){
        return elementDAO.readElement(id);
    }

    @Override
    public void updateElement(Element element) {
        elementDAO.updateElement(element);
    }

    @Override
    public void deleteElement(int id) {
        elementDAO.delElement(id);
    }
}
