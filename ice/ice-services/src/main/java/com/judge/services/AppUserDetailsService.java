package com.judge.services;

import com.judge.model.DAO.IUserDAO;
import com.judge.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Created by Aliaksei_Lapanau on 21.05.2017.
 */
@Service
//@Transactional(readOnly = true)
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserDAO iUserDAO;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = iUserDAO.readUser(login);
        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().toString());
        UserDetails userDetails = (UserDetails)new org.springframework.security.core.userdetails.User(user.getLogin(),
                user.getPassword(), Arrays.asList(authority));
        return userDetails;
    }
}
