package com.judge.services;

import com.judge.model.DAO.IUserDAO;
import com.judge.model.User;
import com.judge.services.interfaces.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Aliaksei_Lapanau on 27.05.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    //
    @Mock
    IUserDAO userDAO;

    @InjectMocks
    IUserService us = new UserService();

    @Test
    public void getListUsers() throws Exception {

        List users = new ArrayList();//mock(List.class);
        users.add(new User());

        when(userDAO.getListUsers()).thenReturn(users);

        assertEquals(users.size(), us.getListUsers().size());
        assertEquals(users,us.getListUsers());
    }

    @Test
    public void createUser_success() throws Exception {
        User user = new User();
        String login = "user";
        user.setLogin(login);
        when(userDAO.readUser(user.getLogin())).thenReturn(null);
        doNothing().when(userDAO).createUser(user);

        assertTrue(us.createUser(user));
    }

    @Test
    public void createUser_userExist() throws Exception {
        User user = new User();
        String login = "user";
        user.setLogin(login);
        when(userDAO.readUser(user.getLogin())).thenReturn(user);

        assertFalse(us.createUser(user));
    }

    @Test
    public void readUser() throws Exception {

        User user = mock(User.class);

        when(userDAO.readUser(1)).thenReturn(user);

        assertEquals(user,us.readUser(1));


    }

    @Test
    public void updateUser() throws Exception {
        User user = mock(User.class);

        doNothing().when(userDAO).updateUser(user);
        us.updateUser(user);
    }

    @Test
    public void deleteUser() throws Exception {
        User user = mock(User.class);

        doNothing().when(userDAO).delUser(1);
        us.deleteUser(1);
    }


}