package com.judge.model.enums;

/**
 * Created by Aliaksei_Lapanau on 17.05.2017.
 */
public enum UserRole {
    ROLE_ADMIN,
    ROLE_ELEMENTS_EXPERT,
    ROLE_EXECUTION_EXPERT
}
