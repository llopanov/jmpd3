package com.judge.model;

import com.judge.model.enums.UserRole;

import javax.persistence.*;

/**
 * Created by Aliaksei_Lapanau on 10.05.2017.
 */
//@Entity
//@Table(name = "users", catalog = "test")
@Entity
@Table(name="users")
public class User {
    @Id
    @Column(name="id_user")
    private int id_user;

    @Column(name="login")
    private String login;

    @Column(name="password")
    private String password;

    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private UserRole role;

    public User(){

    }

    public User(int id_user, String login, String password, UserRole role) {
        this.id_user = id_user;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public User(String login, String password, UserRole role) {
        this.id_user = 0;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id_user == user.id_user;
    }

    @Override
    public int hashCode() {
        return id_user;
    }
}
