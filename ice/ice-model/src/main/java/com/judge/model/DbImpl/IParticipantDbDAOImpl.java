package com.judge.model.DbImpl;

import com.judge.model.DAO.IParticipantDAO;
import com.judge.model.DAO.IUserDAO;
import com.judge.model.Participant;
import com.judge.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */

@Repository
@Transactional
public class IParticipantDbDAOImpl implements IParticipantDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Participant> getListParticipants() {
        String hql = "FROM Participant as p ORDER BY p.id_participant";
        return (List<Participant>) entityManager.createQuery(hql).getResultList();
    }

//    @Override
//    public void createUser(User user) {
//        entityManager.persist(user);
//    }
//
//    @Override
//    public User readUser(int id) {
//        return entityManager.find(User.class, id);
//    }
//
//    @Override
//    public User readUser(String login) {
//
//        List<?> list = entityManager.createQuery("SELECT u FROM User u WHERE login=:login")
//                .setParameter("login", login).getResultList();//getSingleResult();//
//
//        if(!list.isEmpty()) {
//            return (User)list.get(0);
//        }
//        else {
//            return null;
//        }
//
//}
//
//    @Override
//    public void updateUser(User user) {
//        User usr = readUser(user.getId_user());
//        usr.setLogin(user.getLogin());
//        usr.setPassword(user.getPassword());
//        usr.setRole(user.getRole());
//        entityManager.flush();
//    }
//
//
//
//    @Override
//    public void delUser(int id) {
//        entityManager.remove(readUser(id));
//    }
}
