package com.judge.model.DAO;

import com.judge.model.Participant;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 10.05.2017.
 */
public interface IParticipantDAO {
    List<Participant> getListParticipants();
//    void createParticipant(Participant participant);
//    Participant readParticipant(int id);
//    void updateParticipant(Participant participant);
//    void delParticipant(Participant participant);
}
