package com.judge.model.DAO;

import com.judge.model.Mark;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
public interface MarkDAO {
    void createMark(Mark mark);
    Mark readMark(int id);
    void updateMark(Mark mark);
    List<Mark> listMarks();
    void delMark(Mark mark);
}
