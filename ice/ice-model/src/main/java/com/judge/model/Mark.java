package com.judge.model;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
public class Mark {
    private int id_mark;
    private int id_user; // insted of user name etc
    //private int gradeOfExec;
    private double execution;

    public Mark(int id_mark, int id_user, double execution) {
        this.id_mark = id_mark;
        this.id_user = id_user;
        this.execution = execution;
    }

    public int getId_mark() {
        return id_mark;
    }

    public void setId_mark(int id_mark) {
        this.id_mark = id_mark;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public double getExecution() {
        return execution;
    }

    public void setExecution(double execution) {
        this.execution = execution;
    }
}
