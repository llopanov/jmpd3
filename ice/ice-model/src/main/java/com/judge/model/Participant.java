package com.judge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Aliaksei_Lapanau on 11.05.2017.
 */
@Entity
@Table(name="participants")
public class Participant {
    @Id
    @Column(name="id_participant")
    private int id_participant;

    @Column(name="name")
    private String name;

    @Column(name="photo")
    private String photo;

    public Participant() {
    }

    public Participant(int id_participant, String name, String photo) {
        this.id_participant = id_participant;
        this.name = name;
        this.photo = photo;
    }

    public Participant(String name, String photo) {
        this.id_participant = 0;
        this.name = name;
        this.photo = photo;
    }

    public int getId_participant() {
        return id_participant;
    }

    public void setId_participant(int id_participant) {
        this.id_participant = id_participant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    //private String country;
    
}
