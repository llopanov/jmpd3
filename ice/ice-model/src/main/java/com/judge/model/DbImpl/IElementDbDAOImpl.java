package com.judge.model.DbImpl;

import com.judge.model.DAO.IElementDAO;
import com.judge.model.Element;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 28.05.2017.
 */
@Repository
@Transactional
public class IElementDbDAOImpl implements IElementDAO{
    

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createElement(Element element) {
        entityManager.persist(element);
    }

    @Override
    public Element readElement(int id) {
        return entityManager.find(Element.class, id);
    }

    @Override
    public void updateElement(Element element) {
        Element elem = readElement(element.getId_element());
        elem.setName(element.getName());
        elem.setBaseValue(element.getBaseValue());
        entityManager.flush();
    }

    @Override
    public void delElement(int id) {
        entityManager.remove(readElement(id));
    }

    @Override
    public List<Element> getListElements() {
        String hql = "FROM Element as e ORDER BY e.id_element";
        return (List<Element>) entityManager.createQuery(hql).getResultList();
    }

}
