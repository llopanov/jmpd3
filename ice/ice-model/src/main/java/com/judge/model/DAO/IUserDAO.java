package com.judge.model.DAO;

import com.judge.model.User;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 10.05.2017.
 */
public interface IUserDAO {
    void createUser(User user);
    User readUser(int id);
    User readUser(String login);
    void updateUser(User user);
    List<User> getListUsers();
    void delUser(int id);
}
