package com.judge.model.DAO;

import com.judge.model.Element;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
public interface IElementDAO {
    void createElement(Element element);
    Element readElement(int id);
    void updateElement(Element element);
    List<Element> getListElements();
    void delElement(int id);
}
