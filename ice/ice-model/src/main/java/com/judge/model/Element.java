package com.judge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Aliaksei_Lapanau on 13.05.2017.
 */
@Entity
@Table(name="elements")
public class Element {
    @Id
    @Column(name="id_element")
    private int id_element;

    @Column(name="name")
    private String name;

    @Column(name="baseValue")
    private double baseValue;

    public Element(){

    }

    public Element(int id_element, String name, double baseValue) {
        this.id_element = id_element;
        this.name = name;
        this.baseValue = baseValue;
    }

    public Element(String name, double baseValue) {
        this.id_element = 0;
        this.name = name;
        this.baseValue = baseValue;
    }

    public int getId_element() {
        return id_element;
    }

    public void setId_element(int id_element) {
        this.id_element = id_element;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(double baseValue) {
        this.baseValue = baseValue;
    }
}
