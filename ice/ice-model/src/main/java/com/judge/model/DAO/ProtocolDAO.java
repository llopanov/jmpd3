package com.judge.model.DAO;

import com.judge.model.Protocol;

import java.util.List;

/**
 * Created by Aliaksei_Lapanau on 10.05.2017.
 */
public interface ProtocolDAO {
    void createProtocol(Protocol protocol);
    Protocol readProtocol(int id);
    void updateProtocol(Protocol protocol);
    List<Protocol> listProtocols();
    void delProtocol(Protocol protocol);
}
