package com.judge.model;

import java.util.List;
import java.util.Map;

/**
 * Created by Aliaksei_Lapanau on 14.05.2017.
 */
public class Protocol {
    private int id_protocol;
    private Map<Element, List<Mark>> estimations;
    private Participant participant;
    private double deductions; // penalties

    //private List skatingSkills; // for every judge
    //private List linkinFootwork; // for every judge
    //private List<Double> execution; // for every judge
    //private List composition; // for every judge
    //private List interpretation; // for every judge

    public Protocol(int id_protocol, Map<Element, List<Mark>> estimations, Participant participant, double deductions) {
        this.id_protocol = id_protocol;
        this.estimations = estimations;
        this.participant = participant;
        this.deductions = deductions;
    }

    public Protocol(Map<Element, List<Mark>> estimations, Participant participant, double deductions) {
        this.id_protocol = 0;
        this.estimations = estimations;
        this.participant = participant;
        this.deductions = deductions;
    }

    public int getId_protocol() {
        return id_protocol;
    }

    public void setId_protocol(int id_protocol) {
        this.id_protocol = id_protocol;
    }

    public Map<Element, List<Mark>> getEstimations() {
        return estimations;
    }

    public void setEstimations(Map<Element, List<Mark>> estimations) {
        this.estimations = estimations;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public double getDeductions() {
        return deductions;
    }

    public void setDeductions(double deductions) {
        this.deductions = deductions;
    }
}
