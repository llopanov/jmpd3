
import com.mongodb.*;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.print.Doc;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Sorts.orderBy;

/**
 * Created by Aliaksei_Lapanau on 22.06.2017.
 * Write simple app with a few classes reflected typical Social Network (Users, Messages, Movies, Audio tracks, Friendships and etc)
 * Keep more than 10^9 instances for each class into NoSQL database Provide simple reporting.

 1) Average number of messages by day of week

 2) Max number of new friendships from month to month

 3) Min number of watched movies by users with more than 100 friends

 P.S. Play with different data models, don't fear to experiment with denormalization

 Do the same for both MongoDB (use Mongo Aggregation Framework) and Cassandra (build separate tables for each kind of report).
 */
public class Main {
    private static String namesList[] = {"Petya", "Sasha", "Kolya", "Gena", "Vova", "Dima", "Katya", "Masha"};
    private static String mesagesList[] = {
            "Hello",
            "How are you",
            "good by",
            "congratulations",
            "regards",
            "sam durak",
            "privet",
            "poka"
    };

    public static void main(String[] args) {
        MongoClient mongo = new MongoClient(  );
        //auth
//        MongoClient mongoClient = new MongoClient();
//        DB db = mongoClient.getDB("database name");
//        boolean auth = db.authenticate("username", "password".toCharArray());

        MongoDatabase db = mongo.getDatabase("social_network");//getDB("social_network");
        MongoCollection<Document> users = db.getCollection("user"); //"user"
//        MongoCollection<Document> messages = db.getCollection("messages");
//        MongoCollection<Document> movies = db.getCollection("movies");
//        MongoCollection<Document> friendship = db.getCollection("friendship");

       generateData(db,"user", 10); //"user"

        FindIterable<Document> iterable = users.find();//new Document()
        //iterable.forEach((Block<? super Document>) System.out::println);

        System.out.println("\n-------Average number of messages by day of week---------\n");
        // 1) Average number of messages by day of week

        AggregateIterable<Document> byDayOfWeekRes = users.aggregate(Arrays.asList(
                unwind("$messages"),

                group(new Document("$dayOfWeek","$messages.date"),sum("count", new Document("$sum", 1))),
                sort(new Document("_id", 1))
                //new Document("$unwind", "$messages"),
                //new Document("$group", new Document("$messages.date",1))
        ));

        for (Document dbObject : byDayOfWeekRes)
        {
            System.out.println(dbObject);
        }

        System.out.println("\n--------Max number of new friendships from month to month-----------\n");
        // 2) Max number of new friendships from month to month

        AggregateIterable<Document> numFriendshipRes = users.aggregate(Arrays.asList(
                unwind("$friends"),
                group(new Document("$month","$friends.date"),sum("count", new Document("$sum", 1))),
                sort(new Document("count", -1)),
                limit(1)
        ));


        numFriendshipRes.forEach((Block<? super Document>) System.out::println);

        //FindIterable<Document> itmessages =  users.find(new Document("messages", new Document()));
        //itmessages.forEach((Block<? super Document>) System.out::println);


    }

    public static void generateData(MongoDatabase db, String collection, int count){
        MongoCollection<Document> users = db.getCollection(collection);
        users.deleteMany(new Document());


        for (int i = 0; i < count; i++) {
            users.insertOne(getUser());
        }
    }

    public static Document getUser(){
        List<Document> messages = new ArrayList<>();
        for (int i = 0; i < getRandBetween(0,5); i++) {
            Document message = new Document();
            message.put("message", mesagesList[getRandBetween(0,mesagesList.length-1)]);
            message.put("date", getRandDateUtil());
            messages.add(message);
        }

        List<Document> friendships = new ArrayList<>();
        for (int i = 0; i < getRandBetween(0,5); i++) {
            Document friendship = new Document();
            friendship.put("name", namesList[getRandBetween(0, namesList.length-1)]);
            friendship.put("date", getRandDateUtil());
            friendships.add(friendship);
        }

        Document doc = new Document().append("name",namesList[getRandBetween(0, namesList.length-1)])
                .append("friends", friendships)
                .append("messages", messages);

        return doc;
    }

    public static int getRandBetween(int begin, int end){
        return begin + (int)Math.round(Math.random()*(end - begin));
    }

    public static LocalDateTime getRandDate(){
        return LocalDateTime.of(
                getRandBetween(1990 , 2016),
                getRandBetween(1 , 12),
                getRandBetween(1 , 28),
                getRandBetween(0 , 23),
                getRandBetween(0 , 59),
                getRandBetween(0 , 59));
    }

    public static Date getRandDateUtil() {
        LocalDateTime ldt  = getRandDate();
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        return date;
    }

}
