import java.util.Date;

/**
 * Created by Aliaksei_Lapanau on 22.06.2017.
 */
public class Message {
    private int id;
    private String message;
    private int idFrom;
    private int idTo;
    private Date date;
}
