import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Aliaksei_Lapanau on 17.06.2017.
 *
 Создать тестовую java-программу, которая в бесконечном цикле
 - создаёт большое количество объектов разного размера, на какое-то время держит на них референсы
 - объекты должны ссылаться друг на друга (когда создаётся очередная пачка объектов, нужно рандомным образом между ними создать связи)
 - должно быть столько объектов чтобы программе было тяжело работать из-за постоянного заполнения хипа и очистки его GC

 Затюнить памяти, сконфигурировать разные типы GC (CMS, G1, Parallel, Serial), затюнить их параметры.

 С помощью JVM параметров настроить запись лога GC, по логу определить на сколько GC приостанавливал JVM и сколько данных очищалось
 Визуализировать лог с помощью программы gcviewer
 Посмотреть на работу GC с помощью JVisualVM и плагина VisualGC

 Сравнить GC, выбрать какой из них в какиз случаях работает лучше
 Закомитать программу, файлик с параметрами запуска JVM для каждого GC, логи GC, скриншоты из GCViewer и VisualGC

 serial     : -Xmx10m -Xloggc:gcSerial.log -XX:+UseSerialGC
 Parallel   : -Xmx10m -Xloggc:gcParallel.log -XX:+UseParallelGC -XX:+UseParallelOldGC
 CMS        : -Xmx10m -Xloggc:gcCMS.log -XX:+UseConcMarkSweepGC
 G1         : -Xmx10m -Xloggc:gcG1.log -XX:+UseG1GC
 */


public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("hello");
        Random rand = new Random();

        int it = 0;
        Thread.sleep(10000);
        System.out.println("start");
        for(;;){
            List<Object> list = new ArrayList();
            list.add(new Object());
            for (int i = 0; i < 1_000; i++) {
                Object ob = getObject(rand.nextInt(64),list.get(rand.nextInt(list.size())));
                list.add(ob);
            }
            System.out.println("cycle - "  + it++);
            if (it == 150_000){
                break;
            }
        }

    }

    private static Object getObject(int size, Object ref){

        return new Ob(size,ref);
    }

}
