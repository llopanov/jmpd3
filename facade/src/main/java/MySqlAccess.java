import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by antar on 12.03.2017.
 */
public class MySqlAccess implements DbAccess {
    private String className = "org.gjt.mm.mysql.Driver";
    private String dbUrl = "jdbc:mysql://localhost/eszaru";
    private String user = "root";
    private String password = "";

    public Connection getConnection() {
        try {
            Class.forName(className);

            Connection cn = DriverManager.getConnection(dbUrl, user, password);

           return cn;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
