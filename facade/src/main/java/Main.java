import java.sql.Connection;

/**
 * Created by antar on 12.03.2017.
 */
//On the base of Facade implement class, organize the connection to database.
public class Main {
    public static void main(String[] args) {
        System.out.println("hello facade");
        
        DbAccess dbAccess = new MySqlAccess();ConnectionMaker.getConnection("MySQL");
        Connection connection = dbAccess.getConnection();

        System.out.println(connection);
    }
}
