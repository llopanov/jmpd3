import java.sql.*;

/**
 * Created by antar on 12.03.2017.
 */
public class ConnectionMaker{

    static DbAccess getConnection(String type) {
        if ("MySQL".equals(type))
        {
            DbAccess mySqlAccess = new MySqlAccess();
            return mySqlAccess;
        }
        else
        {
            throw new IllegalArgumentException("not supported type of DB");
        }
    }
}
