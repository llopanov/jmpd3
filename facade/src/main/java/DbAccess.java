import java.sql.Connection;

/**
 * Created by antar on 12.03.2017.
 */
public interface DbAccess {
    Connection getConnection();
}
