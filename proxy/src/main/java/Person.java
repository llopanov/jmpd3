/**
 * Created by antar on 05.03.2017.
 */
public class Person {
    private String name;
    private String phone;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }

    private String sex;

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {

        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getSex() {
        return sex;
    }

    public Person(String name, String phone, String sex) {

        this.name = name;
        this.phone = phone;
        this.sex = sex;
    }
}
