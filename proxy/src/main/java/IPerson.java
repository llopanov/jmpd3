/**
 * Created by antar on 05.03.2017.
 */
public interface IPerson {
    Person readPerson(String name);
}
