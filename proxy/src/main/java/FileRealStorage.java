/**
 * Created by antar on 05.03.2017.
 */
public class FileRealStorage implements IPerson {
    public Person readPerson(String name) {
        if ("Vasya".equals(name)) {
            return new Person("Vasya","1232323","M");
        } else {
            return null;
        }
    }
}
