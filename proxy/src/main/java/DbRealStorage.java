/**
 * Created by antar on 05.03.2017.
 */
public class DbRealStorage implements IPerson {
    public Person readPerson(String name) {
        if ("Masha".equals(name)) {
            return new Person("Masha", "5535533", "F");
        } else {
            return null;
        }
    }
}
