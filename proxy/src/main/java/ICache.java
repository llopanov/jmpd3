/**
 * Created by antar on 05.03.2017.
 */
interface ICache {
    Person getPerson(String name);
    void addPerson (String name, Person person);
}
