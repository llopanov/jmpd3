import java.util.HashMap;
import java.util.Map;

/**
 * Created by antar on 05.03.2017.
 */
public class CacheImplementaition implements ICache {
    private Map <String, Person>  personCache = new HashMap();
    public Person getPerson(String name) {
        return personCache.get(name);
    }

    public void addPerson(String name, Person person) {
        System.out.println("add to cache " + name);
        personCache.put(name, person);
    }
}
