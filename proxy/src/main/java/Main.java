/**
 * Created by antar on 05.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("hello proxy");
        IPerson person = new Proxy();
        System.out.println(person.readPerson("Masha"));
        System.out.println(person.readPerson("Masha"));

        System.out.println(person.readPerson("Vasya"));
        System.out.println(person.readPerson("Vasya"));
    }
}
