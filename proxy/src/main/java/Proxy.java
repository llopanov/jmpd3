/**
 * Created by antar on 05.03.2017.
 */
public class Proxy implements IPerson {
    private ICache cache = new CacheImplementaition();
    private IPerson realPerson = new DbRealStorage();
    //private IPerson realPerson = new FileRealStorage();
    public Person readPerson(String name) {
        Person personReturn = cache.getPerson(name);
        if (personReturn == null) {
            System.out.println("read from resource "+ name);
            personReturn = realPerson.readPerson(name);
            cache.addPerson(name, personReturn);
        }
        return personReturn;
    }
}
