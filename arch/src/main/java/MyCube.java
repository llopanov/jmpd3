/**
 * Created by antar on 12.02.2017.
 */
public class MyCube implements Volumable{
    private double v;
    private double a;

    public MyCube(double a) {
        this.a = a;
        this.v = a*a*a;

        System.out.println("original volume is " + this.v);
    }

    public double getDx() {
        return a;
    }

    public double getDy() {
        return a;
    }

    public double getDz() {
        return a;
    }

    public boolean inVolume(double x, double y, double z, double bitSizeX, double bitSizeY, double bitSizeZ) {
        double locX, locY, locZ;
        for (int ix = 0 ;ix<2; ix++){
            for (int iy = 0 ;iy<2; iy++){
                for (int iz = 0 ;iz<2; iz++){
                    locX = x + ix*bitSizeX  ;
                    locY = y + iy*bitSizeY  ;
                    locZ = z + iz*bitSizeZ  ;

                    if (locX < a && locY < a && locZ < a){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
