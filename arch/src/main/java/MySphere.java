import java.math.BigDecimal;

/**
 * Created by antar on 11.02.2017.
 */
public class MySphere implements Volumable{
    private double R;
    private double D;
    private double V;

    public MySphere(double r) {
        R = r;
        D = 2*R;
        V = 4*r*r*r*Math.PI/3;
        System.out.println("original volume is " + V);
    }

    public double getDx() {
        return D;
    }

    public double getDy() {
        return D;
    }

    public double getDz() {
        return D;
    }

    public double getOrigVolume(){
        return V;
    }

    public boolean inVolume(double x, double y, double z, double bitSizeX, double bitSizeY, double bitSizeZ) {
        double locX, locY, locZ;
        double R2 = R*R;
        for (int ix = 0 ;ix<2; ix++){
            for (int iy = 0 ;iy<2; iy++){
                for (int iz = 0 ;iz<2; iz++){
                    locX = x + ix*bitSizeX - R ;
                    locY = y + iy*bitSizeY - R ;
                    locZ = z + iz*bitSizeZ - R ;

                    if ((locX )*(locX ) + (locY )*(locY ) + (locZ )*(locZ ) < R2){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
