import java.math.BigDecimal;

/**
 * Created by antar on 11.02.2017.
 */
public interface Volumable {
    double getDx();
    double getDy();
    double getDz();
    boolean inVolume(double x,double y, double z, double bitSizeX,double bitSizeY, double bitSizeZ);
}
