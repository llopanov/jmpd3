/**
 * Created by antar on 11.02.2017.
 */
public class CalculateVolume {
    private Volumable figure;
    private int accuracy = 1000;

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public CalculateVolume(Volumable figure) {
        this.figure = figure;
    }

    public void setFigure(Volumable figure) {
        this.figure = figure;
    }

    public double calculate(){
        double bitSizeX = figure.getDx()/accuracy;
        double bitSizeY = figure.getDy()/accuracy;
        double bitSizeZ = figure.getDz()/accuracy;

        double bitVolume = bitSizeX*bitSizeY*bitSizeZ;
        double volume = 0;
        long count = 0;
        for (int ix=0; ix<accuracy; ix++){
            for (int iy=0; iy<accuracy; iy++){
                for (int iz=0; iz<accuracy; iz++){
                    count++;
                    if (figure.inVolume(ix*bitSizeX, iy*bitSizeY, iz*bitSizeZ, bitSizeX, bitSizeY, bitSizeZ)){
                        volume+=bitVolume;

                    }
                }
            }
            System.out.println("iteration " +count);
        }
        System.out.println("Total iterations " +count);
        System.out.println("volume is " + volume);
        return volume;
    }
}
