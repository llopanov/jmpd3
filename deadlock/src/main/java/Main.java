/**
 * Created by antar on 15.02.2017.
 */
public class Main {
    static class ThreadForDeadlock implements Runnable {
        private int numThread;
        private Object resourseA;
        private Object resourseB;

        public ThreadForDeadlock(int numThread, Object resourseA, Object resourseB) {
            this.numThread = numThread;
            this.resourseA = resourseA;
            this.resourseB = resourseB;
        }

        public void run() {
            System.out.println("Run thread " +numThread);
            synchronized (resourseA){
                System.out.println("Thread " + numThread+ " take resourse " + resourseA);
                try{
                    Thread.sleep(1000);
                }catch (InterruptedException e){}
                System.out.println("Thread " + numThread+ " is trying to take resourse " + resourseB);
                synchronized (resourseB){
                    System.out.println("Thread " + numThread+ " take resourse " + resourseB);
                }
            }
            System.out.println("Terminate thread " + numThread);
        }
    }

    public static void main(String[] args) {
        Object resourses[] = {
                new Object(),
                new Object(),
                new Object(),
                new Object()
        };
        Thread threads[] = {
                new Thread(new ThreadForDeadlock(1,resourses[0], resourses[1])),
                new Thread(new  ThreadForDeadlock(2,resourses[1], resourses[2])),
                new Thread(new  ThreadForDeadlock(3,resourses[2], resourses[3])),
                new Thread(new  ThreadForDeadlock(4,resourses[3], resourses[0]))
        };

        System.out.println("Run main threads");
        for ( Thread thread :   threads) {
            thread.start();
        }

        System.out.println("End main thread");

    }
}
