Class Name                                                                    | Shallow Heap | Retained Heap | Percentage
--------------------------------------------------------------------------------------------------------------------------
                                                                              |              |               |           
java.util.ArrayList @ 0xc0004c58 Native Stack                                 |           24 |        84 408 |     18,55%
'- java.lang.Object[21079] @ 0xc0098c18                                       |       84 336 |        84 384 |     18,54%
   '- java.lang.String @ 0xc00475c0  qwe                                      |           24 |            48 |      0,01%
      '- char[3] @ 0xc00475d8  qwe                                            |           24 |            24 |      0,01%
class java.lang.System @ 0xc00062b0 System Class, Native Stack                |           32 |        35 768 |      7,86%
class java.util.ResourceBundle @ 0xc001fc00 System Class                      |           24 |        26 664 |      5,86%
java.io.PrintStream @ 0xc0004dd8                                              |           32 |        25 048 |      5,50%
sun.misc.Launcher$AppClassLoader @ 0xc0007120                                 |           88 |        22 992 |      5,05%
class java.nio.charset.Charset @ 0xc00333f8 System Class                      |           24 |        20 448 |      4,49%
class sun.util.locale.provider.LocaleProviderAdapter @ 0xc0022bf0 System Class|           40 |        14 768 |      3,25%
sun.misc.Launcher$ExtClassLoader @ 0xc0007180                                 |           80 |        13 800 |      3,03%
class java.security.Security @ 0xc001e1c0 System Class                        |           16 |        13 560 |      2,98%
class java.io.File @ 0xc002b540 System Class, Native Stack                    |           48 |        13 136 |      2,89%
java.util.HashSet @ 0xc00727b8                                                |           16 |        11 536 |      2,54%
class java.lang.ProcessEnvironment @ 0xc0028bb8 System Class                  |           32 |         9 160 |      2,01%
class java.util.regex.Pattern$CharPropertyNames @ 0xc0026158 System Class     |            8 |         8 240 |      1,81%
class sun.util.locale.provider.LocaleDataMetaInfo @ 0xc0022658 System Class   |            8 |         7 112 |      1,56%
class java.util.Currency @ 0xc001b790 System Class                            |          144 |         6 240 |      1,37%
class sun.util.locale.BaseLocale @ 0xc002a580 System Class                    |            8 |         6 048 |      1,33%
class java.lang.Integer$IntegerCache @ 0xc002bf48 System Class                |           16 |         5 120 |      1,13%
class sun.misc.Launcher$BootClassPathHolder @ 0xc001f778 System Class         |            8 |         5 096 |      1,12%
java.util.Scanner @ 0xc0004ac0 Native Stack                                   |          136 |         5 080 |      1,12%
class sun.util.locale.LanguageTag @ 0xc0022570 System Class                   |           24 |         5 080 |      1,12%
class sun.nio.cs.StandardCharsets @ 0xc0033220 System Class                   |          160 |         4 320 |      0,95%
class sun.nio.cs.MS1251 @ 0xc0032588 System Class                             |           16 |         4 200 |      0,92%
class sun.misc.MetaIndex @ 0xc002b400 System Class                            |            8 |         4 160 |      0,91%
Total: 23 of 1 128 entries; 1 105 more                                        |              |               |           
--------------------------------------------------------------------------------------------------------------------------
