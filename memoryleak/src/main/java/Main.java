import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by antar on 19.02.2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("hello memory leak");
        File file = new File("./Task #5 - Data.txt");
        Scanner sc = null;
        List<String> listCharLines = new ArrayList();
        try {
            sc = new Scanner(new FileReader(file));

            for (; sc.hasNext(); ) {
                String line = sc.nextLine();
                String line3char = line.substring(0,3);
                listCharLines.add(line3char.intern());
            }
            System.out.println("String count = " + listCharLines.size());
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (sc != null){
                sc.close();
            }
        }
    }
}
