package perm;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antar on 26.02.2017.
 */
public class TestThread extends Thread {
    private List<IThreadLeakReason> items = new ArrayList<IThreadLeakReason>();


    @Override
    public void run() {
        IThreadLeakReason obOld = null;
        for (int i=0;i<1000000;i++) {

            URLClassLoader tmp =
                    new URLClassLoader(new URL[]{getClassPath()}) {
                        public Class<?> loadClass(String name)
                                throws ClassNotFoundException {
                            if ("perm.ThreadMemoryLeak".equals(name))
                                return findClass(name);
                            return super.loadClass(name);
                        }
                    };
            //items.add(ob);
            try {
                //URLClassLoader tmp = new MyURLClassLoader(new URL[] {getClassPath()});
                IThreadLeakReason ob = (IThreadLeakReason)tmp.loadClass("perm.ThreadMemoryLeak").newInstance();
                ob.setName(ob.getClass() + " "+ob.getClass().getClassLoader() +" "+ items.size());
                ob.setLnk(obOld);
                System.out.println(i);
                items.add(ob);
                obOld = ob;
    //Thread.sleep(2000);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        }


        //init items by using custom classloader
        // ...
        //clean all references on loaded classes
        //and classloaders
        //cleanClassloadersReferences();
    }

    private static URL getClassPath() {
//    String resName =
//      heap.ExampleFactory.class.getName().replace('.', '/') + ".class";
//    String loc =
//      heap.ExampleFactory.class.getClassLoader().getResource(resName)
//      .toExternalForm();
//    String dir = loc.substring(0, loc.length() - resName.length());

        //String dir = "file:/Users/ekabanov/Documents/workspace-javazone/cl-reload/";
        String dir = "file:E:/jmpd3/errors/target/classes/";

        try {
            return new URL(dir);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
