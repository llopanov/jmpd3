package perm;

/**
 * Created by antar on 26.02.2017.
 */
public class ThreadMemoryLeak implements IThreadLeakReason {
    public static final double[] LEAK_REASON = new double[1000000];
    private double[] doItAgain = LEAK_REASON;
    IThreadLeakReason lnk;

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setLnk(IThreadLeakReason lnk) {
        this.lnk = lnk;
    }
}
