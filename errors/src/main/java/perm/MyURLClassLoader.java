package perm;

import java.net.URL;

/**
 * Created by antar on 26.02.2017.
 */
public class MyURLClassLoader extends java.net.URLClassLoader {


    public MyURLClassLoader(URL[] urls) {
        super(urls);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if ("perm.TestThread".equals(name))
            return findClass(name);
        return super.loadClass(name);

    }
}
