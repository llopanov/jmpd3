package heap;

/**
 * Created by antar on 26.02.2017.
 */
public interface IExample {
    String message();
    int plusPlus();

    int counter();
    IExample copy(IExample example);
    ILeak leak();
}