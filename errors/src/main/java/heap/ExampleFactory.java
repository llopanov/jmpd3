package heap; /**
 * Created by antar on 26.02.2017.
 */
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ExampleFactory {
    public static IExample newInstance() {
        try {
            return newInstanceWithThrows();
        } catch (InstantiationException e) {
            throw new RuntimeException(e.getCause());
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static IExample newInstanceWithThrows() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        URLClassLoader tmp =
                new URLClassLoader(new URL[] {getClassPath()}) {
                    public Class<?> loadClass(String name)
                            throws ClassNotFoundException {
                        if ("heap.Example".equals(name)
                                || "heap.Leak".equals(name))
                            return findClass(name);
                        return super.loadClass(name);
                    }
                };

        return (IExample) tmp.loadClass("heap.Example")
                .newInstance();
    }

    private static URL getClassPath() {
//    String resName =
//      heap.ExampleFactory.class.getName().replace('.', '/') + ".class";
//    String loc =
//      heap.ExampleFactory.class.getClassLoader().getResource(resName)
//      .toExternalForm();
//    String dir = loc.substring(0, loc.length() - resName.length());

        //String dir = "file:/Users/ekabanov/Documents/workspace-javazone/cl-reload/";
        String dir = "file:E:/jmpd3/errors/target/classes/";

        try {
            return new URL(dir);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
