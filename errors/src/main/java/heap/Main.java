package heap;

/**
 * Created by antar on 26.02.2017.
 */
/*
* Implement code that will introduce:


1) java.lang.OutOfMemoryError: Java heap space. Do not use arrays or collections.
2) java.lang.OutOfMemoryError: PermGen space. Load classes continuously and make them stay in memory
3) java.lang.StackOverflowError. Do not use recursive methods.
* */

public class Main {
    private static IExample example1;
    private static IExample example2;

    public static void main(String[] args) throws InterruptedException {
        example1 = ExampleFactory.newInstance();

        while (true) {
            example2 = ExampleFactory.newInstance().copy(example2);

            System.out.println("1) " +
                    example1.message() + " = " + example1.plusPlus());
            System.out.println("2) " +
                    example2.message() + " = " + example2.plusPlus());
            System.out.println();

            Thread.currentThread().sleep(100);
        }
    }
}


//public class heap.Main {
//    public static void main(String[] args) {
//        //heapSpaceError();
//        //permGenError();
//        stackOverflowError();
//    }
//
//    private static void stackOverflowError() {
//        Thread thr[] = new Thread[50];
//        for(int i =0 ; i<thr.length; i++) {
//            Thread thr = new Thread(new SomeThread());
//            thr.start();
//        }
//    }
//
//    private static void permGenError() {
//    }
//
//    private static void heapSpaceError() {
//
//    }
//
//    private static class SomeThread implements Runnable{
//
//        public void run() {
//            try {
//                Thread.sleep(25000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}
