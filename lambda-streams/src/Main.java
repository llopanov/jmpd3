import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Main {

    public static void main(String[] args) {
        //System.out.println("Hello World!");
        List<Point> listRand = generatePoints();
        listRand.stream().forEach(System.out::println);

//        Random random = new Random();
//        random.ints().limit(10).forEach(Point::new(random.nextGaussian(),random.nextGaussian()));


        Function<Point, NewPoint> NewPointFromPoint = NewPoint::new;

        List<NewPoint> listNewPoints =  listRand.stream().map(NewPointFromPoint)
                .filter(el -> (el.getA()<3 && el.getB()<3))
                .distinct().collect(toList());

        System.out.println("=== after removing < 3 and repeatable === count - " + listNewPoints.size());
        listNewPoints.forEach(System.out::println);
        System.out.println("=== Sum ===");
        System.out.println(listNewPoints.stream().map(el -> el.getA()+el.getB()).reduce(0,Integer::sum));
        System.out.println("=== Multiply ===");
        System.out.println(listNewPoints.stream().map(el -> el.getA()*el.getB()).reduce(0,Integer::sum));
    }

    public static List<Point> generatePoints(){
        List<Point> retList = new ArrayList<>();

        for(int i=0; i<10; i++){
            Point point = new Point((int)(Math.random()*10),(int)(Math.random()*10));
            retList.add(point);
        }
        return retList;
    }
}
