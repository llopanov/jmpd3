/**
 * Created by Aliaksei_Lapanau on 04.06.2017.
 */
public class NewPoint {
    private int a;
    private int b;

    public NewPoint(Point pnt) {

        this.a = pnt.getX();
        this.b = pnt.getY();
    }

    public NewPoint(int a, int b) {

        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "NewPoint{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
