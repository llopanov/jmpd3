/**
 * Created by Aliaksei_Lapanau on 26.04.2017.
 */
public class User {
    private int id_user;
    private String login;
    private String password;
    private int type;

    public User(int id_user, String login, String password, int type) {
        this.id_user = id_user;
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
